#!/bin/bash

cd /var/local/app/

source .venv/bin/activate

export PYTHONPATH=$PYTHONPATH:/var/local/app/

python ale-snake-basket/main.py

deactivate

